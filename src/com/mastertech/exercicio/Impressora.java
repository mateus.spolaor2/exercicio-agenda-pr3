package com.mastertech.exercicio;

public class Impressora {

    Mensagens mensagem = new Mensagens();

    public void imprimeMensagem (String s) {
        System.out.println(s);
    }


    public void imprimeContato(Pessoa contato) {
        System.out.println(
                "Contato encontrado! \n" +
                "Email: " + contato.getEmail() +
                "\nTelefone: " + contato.getTelefone());
    }

    public void imprimeMensagemEmail(Integer opcao) {
        if (opcao == 1) {
            System.out.println(mensagem.INSERIR_EMAIL + " que deseja adicionar");
        } else if (opcao == 2){
            System.out.println(mensagem.INSERIR_EMAIL + " que deseja consultar");
        } else {
            System.out.println(mensagem.INSERIR_EMAIL + " que deseja remover");
        }
    }

    public void imprimeMensagemTelefone (Integer opcao){
        if (opcao == 1) {
            System.out.println(mensagem.INSERIR_TELEFONE + " que deseja adicionar");
        } else if (opcao == 2) {
            System.out.println(mensagem.INSERIR_TELEFONE + " que deseja consultar");
        } else {
            System.out.println(mensagem.INSERIR_TELEFONE + " que deseja remover");
        }
    }
}
