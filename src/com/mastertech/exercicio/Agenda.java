package com.mastertech.exercicio;

import java.util.ArrayList;

public class Agenda{

    private Integer qtdContatosMaximo;
    private ArrayList<Pessoa> pessoas = new ArrayList<>();

    public Agenda(Integer qtdContatosMaximo, ArrayList<Pessoa> pessoas) {
        this.qtdContatosMaximo = qtdContatosMaximo;
        this.pessoas = pessoas;
    }

    public Agenda(){}

    public Integer getQtdContatosMaximo() {
        return qtdContatosMaximo;
    }

    public void setQtdContatosMaximo(Integer qtdContatosMaximo) {
        this.qtdContatosMaximo = qtdContatosMaximo;
    }

    public ArrayList<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(ArrayList<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public Pessoa buscaContatoPorEmail(String email){
        for (Pessoa pessoa : this.pessoas) {
            if (pessoa.getEmail().equals(email)) {
                pessoa.setId(pessoas.indexOf(pessoa));
                return pessoa;
            }
        }
        return null;
    }

    public Pessoa buscaContatoPorTelefone(String telefone){
        for (Pessoa pessoa : this.pessoas) {
            if (pessoa.getTelefone().equals(telefone)) {
                pessoa.setId(pessoas.indexOf(pessoa));
                return pessoa;
            }
        }
        return null;
    }

    public boolean add(Pessoa contato) {
        Mensagens mensagens = new Mensagens();
        Impressora impressora = new Impressora();
        if(this.pessoas != null && this.pessoas.size() > this.qtdContatosMaximo ){
            return false;
        }
        return this.pessoas.add(contato);
    }
}
