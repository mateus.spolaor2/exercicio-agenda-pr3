package com.mastertech.exercicio;

import java.util.Scanner;

public class Processador {

    Impressora impressora = new Impressora();
    Mensagens mensagens = new Mensagens();
    Agenda agenda = new Agenda();
    Scanner scannerDados = new Scanner(System.in);

    private Boolean ligado = true;

    public Integer ligaSistema() {
        Scanner scanner = new Scanner(System.in);

        impressora.imprimeMensagem(
                mensagens.BEM_VINDO +
                "\n" +
                mensagens.QTD_MAXIMA_PESSOAS_AGENDA);

        Integer qtdMaxPessoas = scanner.nextInt();
        return qtdMaxPessoas;
    }

    public void executaAgenda(Integer qtdMembrosAgenda) {

        agenda.setQtdContatosMaximo(qtdMembrosAgenda);
        Scanner scannerOpcao = new Scanner(System.in);

        impressora.imprimeMensagem(mensagens.OPCOES);
        int opcao = scannerOpcao.nextInt();
        
        if(opcao == 1){
            adicionaContato(opcao);
        } else if(opcao == 2) {
            apresentarContato(opcao);
        } else if (opcao == 3){
            removerContato(opcao);
        } else {
            desligarSistema();
        }
    }

    public void adicionaContato(Integer opcaoEscolhida) {
        Scanner scannerDados = new Scanner(System.in);

        impressora.imprimeMensagemEmail(opcaoEscolhida);
        String email = scannerDados.nextLine();
        impressora.imprimeMensagemTelefone(opcaoEscolhida);
        String telefone = scannerDados.nextLine();

        Pessoa contato = new Pessoa(email, telefone);

        if(agenda.add(contato)) {
            impressora.imprimeMensagem(mensagens.INSERCAO_OK);
        } else {
            impressora.imprimeMensagem(
                    mensagens.adicaoFalhou(agenda.getQtdContatosMaximo())
            );
        }
    }

    public void removerContato(Integer opcaoEscolhida) {
        Pessoa contato = buscaContato(opcaoEscolhida);
        agenda.getPessoas().remove(contato.getId());
    }

    public void apresentarContato(Integer opcaoEscolhida) {
        Pessoa contato = buscaContato(opcaoEscolhida);
        impressora.imprimeContato(contato);
    }

    public Pessoa buscaContato(Integer opcaoEscolhida) {
        Scanner scannerDados = new Scanner(System.in);
        Pessoa contatoPesquisado;

        impressora.imprimeMensagemEmail(opcaoEscolhida);
        String email = scannerDados.nextLine();

        contatoPesquisado = agenda.buscaContatoPorEmail(email);

        if(contatoPesquisado == null){

            impressora.imprimeMensagem(mensagens.CONTATO_NAO_ENCONTRADO_EMAIL);
            impressora.imprimeMensagemTelefone(opcaoEscolhida);

            String telefone = scannerDados.nextLine();

            contatoPesquisado = agenda.buscaContatoPorTelefone(telefone);

            if(contatoPesquisado == null) {
                impressora.imprimeMensagem(mensagens.CONTATO_NAO_ENCONTRADO);
                return null;
            } else {
                return contatoPesquisado;
            }
        } else {
            return contatoPesquisado;
        }
    }

    private void desligarSistema() {
        impressora.imprimeMensagem(mensagens.DESLIGAR_SISTEMA);
        this.ligado = false;
        System.exit(0);
    }

    public boolean isOn() {
        return this.ligado;
    }
}
