package com.mastertech.exercicio;

public class Main {

    public static void main(String[] args) {

        Processador processador = new Processador();

        Integer qtdMaxAgenda = processador.ligaSistema();

        while(processador.isOn()){
            processador.executaAgenda(qtdMaxAgenda);
        }
    }
}
