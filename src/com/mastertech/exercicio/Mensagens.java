package com.mastertech.exercicio;

public class Mensagens {

    public final String BEM_VINDO = "Bem vindo à agenda";

    public final String QTD_MAXIMA_PESSOAS_AGENDA = "Quantas pessoas deseja armazenar em sua agenda?";

    public final String OPCOES =
            "Selecione a funcionalidade: \n" +
            "1) Insere membro na agenda \n" +
            "2) Busca de membro por email ou telefone \n" +
            "3) Remoção de membro da agenda \n" +
            "4) Desligar a agenda";

    public final String INSERIR_EMAIL = "Insira o email";

    public final String INSERIR_TELEFONE = "Insira o telefone";

    public final String DESLIGAR_SISTEMA = "Obrigado por usar a agenda. \nAté logo! ;)";

    public final String INSERCAO_OK = "Contato adicionado com sucesso ;)";

    public final String CONTATO_NAO_ENCONTRADO_EMAIL = "Contato não encontrado por email";

    public final String CONTATO_NAO_ENCONTRADO = "Contrato não encontrado nem por email nem por telefone =(";

    public final String adicaoFalhou(Integer i) {
        return "Você não pode adicionar mais contatos à sua agenda. \n" +
                "Seu limite é de: " + i;
    }



}
